package ru.inshakov.tm.command.auth;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.util.TerminalUtil;

public class RegistryCommand extends AbstractCommand {
    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new user";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("Enter email");
        @Nullable final String email = TerminalUtil.nextLine();
        serviceLocator.getSessionEndpoint().register(login, password, email);
    }
}
