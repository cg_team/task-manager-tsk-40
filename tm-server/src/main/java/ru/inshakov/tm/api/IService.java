package ru.inshakov.tm.api;

import ru.inshakov.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
