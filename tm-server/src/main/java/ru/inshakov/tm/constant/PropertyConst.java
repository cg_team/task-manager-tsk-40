package ru.inshakov.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface PropertyConst {

    @NotNull
    String FILE_NAME = "application.properties";

    @NotNull
    String PASSWORD_SECRET_KEY = "secret";

    @NotNull
    String PASSWORD_SECRET_VALUE = "";

    @NotNull
    String PASSWORD_ITERATION_KEY = "iteration";

    @NotNull
    String PASSWORD_ITERATION_VALUE = "1";

    @NotNull
    String APPLICATION_VERSION_KEY = "version";

    @NotNull
    String APPLICATION_VERSION_VALUE = "0.25.0";

    @NotNull
    String SERVER_HOST_KEY = "host";

    @NotNull
    String SERVER_HOST_VALUE = "localhost";

    @NotNull
    String SERVER_PORT_KEY = "port";

    @NotNull
    String SERVER_PORT_VALUE = "8080";

    @NotNull
    String SIGNATURE_SECRET_KEY = "sign.secret";

    @NotNull
    String SIGNATURE_SECRET_VALUE = "";

    @NotNull
    String SIGNATURE_ITERATION_KEY = "sign.iteration";

    @NotNull
    String SIGNATURE_ITERATION_VALUE = "1";

    @NotNull
    String JDBC_USER_KEY = "jdbc.user";

    @NotNull
    String JDBC_PASSWORD_KEY = "jdbc.password";

    @NotNull
    String JDBC_URL_KEY = "jdbc.url";
    
    @NotNull
    String JDBC_DRIVER_KEY = "jdbc.driver";

}
