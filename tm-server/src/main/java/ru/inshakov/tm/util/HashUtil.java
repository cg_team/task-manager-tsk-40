package ru.inshakov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.other.ISaltSetting;
import ru.inshakov.tm.api.other.ISignatureSetting;

import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @Nullable
    static String salt(
            @Nullable final ISaltSetting setting,
            @Nullable final String value
    ) {
        if (setting == null) return null;
        @Nullable final String secret = setting.getPasswordSecret();
        @Nullable final Integer iteration = setting.getPasswordIteration();
        return salt(secret, iteration, value);
    }

    @Nullable
    static String sign(@NotNull final ISignatureSetting setting, @NotNull final Object value) {
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            return salt(json, setting.getSignatureIteration(), setting.getSignatureSecret());
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    static String salt(
            @Nullable final String secret,
            @Nullable final Integer iteration,
            @Nullable final String value
    ) {
        if (secret == null) return null;
        if (iteration == null) return null;
        if (value == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @Nullable
    static String md5(@Nullable String value) {
        if (value == null) return null;
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
