package ru.inshakov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.repository.IProjectRepository;
import ru.inshakov.tm.api.repository.ISessionRepository;
import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.api.repository.IUserRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.exception.system.ConnectionFailedException;

import javax.sql.DataSource;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        sqlSessionFactory = getSqlSessionFactory();
    }

    @NotNull
    @Override
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    private SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String driver = propertyService.getJdbcDriver();
        if (driver == null) throw new ConnectionFailedException();
        @Nullable final String username = propertyService.getJdbcUser();
        if (username == null) throw new ConnectionFailedException();
        @Nullable final String password = propertyService.getJdbcPassword();
        if (password == null) throw new ConnectionFailedException();
        @Nullable final String url = propertyService.getJdbcUrl();
        if (url == null) throw new ConnectionFailedException();

        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
